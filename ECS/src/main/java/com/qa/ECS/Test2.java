package com.qa.ECS;

public class Test2 {

	static int findElement(int arr[], int n)
	{

		int[] prefixSum = new int[n];
		prefixSum[0] = arr[0];
		for (int i = 1; i < n; i++)
			prefixSum[i] = prefixSum[i - 1] + arr[i];

		// Forming suffix sum array from n-1
		int[] suffixSum = new int[n];
		suffixSum[n - 1] = arr[n - 1];
		for (int i = n - 2; i >= 0; i--)
			suffixSum[i] = suffixSum[i + 1] + arr[i];

		// Find the point where prefix and suffix
		// sums are same.
		for (int i = 1; i < n - 1; i++)
			if (prefixSum[i] == suffixSum[i])
				return i;

		return -1;

	}


	public static void main(String args[])
	{
	//	int arr[] = { 133,60,23,92,6,7,168,16,19 };
		int arr[] = { 30,43,29,10,50,40,99,51,12 };
		
		int n = arr.length;
		int result = findElement(arr, n);
		System.out.println("Index:"+result);

		
		
}


}
