package utils;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.TestBase;

public class TestUtils extends TestBase {

	public static Integer getIndex(List<WebElement> rows){

		ArrayList<Integer> list = new ArrayList<Integer>();

		for(WebElement ele: rows){
			list.add(Integer.valueOf(ele.getText()));
		}
		int n=list.size();

		int[] prefixSum = new int[n];
		prefixSum[0] = list.get(0);
		for (int i = 1; i < n; i++)
			prefixSum[i] = prefixSum[i - 1] + list.get(i);


		int[] suffixSum = new int[n];
		suffixSum[n - 1] = list.get(n-1);
		for (int i = n - 2; i >= 0; i--)
			suffixSum[i] = suffixSum[i + 1] + list.get(i);

		for (int i = 1; i < n - 1; i++)
			if (prefixSum[i] == suffixSum[i]){ 
				return i;
			}
		return -1;

	}
	
	
	public static int getColumnSize() {
		List<WebElement> rowss= driver.findElements(By.cssSelector("table>tbody>tr"));
		int column = 0;
		 for(WebElement row: rowss)
		    {
			 List<WebElement> columnS = row.findElements(By.tagName("td"));
			 column = columnS.size();
		    }
		return column;
	}
	
	public static List<WebElement> getRowElements(String row) {
		List<WebElement> rowElements = new ArrayList<WebElement>();
		int column=TestUtils.getColumnSize();
		WebElement ele = null;
		for(int i=0;i<column;i++){
			ele = driver.findElement(By.xpath("//*[@data-test-id='"+row+i+"']"));
		rowElements.add(ele);
		}
		return rowElements;
	}


}
