package stepDefination;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.TestBase;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.TestUtils;

public class HomeStepDefination extends TestBase{

	@Before
    public void beforeScenario(){
   //System.out.println("This will run before the Scenario");
   //start the app using yarn && yarn start
    }	
	
	
	@Given("^I am on ECSDigital page$")
	public void i_am_on_ECSDigital_page() {
		driver.get("http://localhost:3000/");	
	}

	@When("^I click on RenderTheChallenege$")
	public void i_click_on_RenderTheChallenege()  {
		driver.findElement(By.xpath("//span[contains(text(),'Render the Challenge')]")).click();
	}

	@And("^I scroll down to Challange and pick firstRow$")
	public void i_scroll_down_to_Challange_and_pick_firstRow()  {
		 
		String row1="array-item-1-";
		
		List<WebElement> row1Elements = new ArrayList<WebElement>();
		row1Elements= TestUtils.getRowElements(row1);
		
		Integer index = TestUtils.getIndex(row1Elements);
		System.out.println(driver.findElement(By.xpath("//*[@data-test-id='submit-1']")).getText());
		
		driver.findElement(By.xpath("//*[@data-test-id='submit-1']")).sendKeys(index.toString());;

	}

	@And("^I pick the secondRow$")
	public void i_pick_secondRow() throws Throwable {
		
		String row2="array-item-2-";
		
		List<WebElement> row2Elements = new ArrayList<WebElement>();
		row2Elements= TestUtils.getRowElements(row2);

		Integer index = TestUtils.getIndex(row2Elements);

		driver.findElement(By.xpath("//*[@data-test-id='submit-2']")).sendKeys(index.toString());;
	}

	@And("^I pick the thirdRow$")
	public void i_pick_thirdRow() throws Throwable {
		
		String row3="array-item-3-";
		List<WebElement> row3Elements = new ArrayList<WebElement>();
		
		row3Elements = TestUtils.getRowElements(row3);

		Integer index = TestUtils.getIndex(row3Elements);

		driver.findElement(By.xpath("//*[@data-test-id='submit-3']")).sendKeys(index.toString());;
	}


	@Then("^I enter results$")
	public void i_enter_results() throws Throwable {
		driver.findElement(By.xpath("//*[@data-test-id='submit-4']")).sendKeys("Seema");
	}

	@Then("^I click submit button$")
	public void i_click_submit_button() throws Throwable {	
		driver.findElement(By.xpath("//span[contains(text(),'Submit Answers')]")).click();	
	}
	
	
	@After
    public void afterScenario(){
    //System.out.println("This will run after the Scenario");
	//driver.quit();
    }
	
	

}